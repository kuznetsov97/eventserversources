﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace EventServer.Models
{
    public class CategoryEvent
    {
        public int Id { get; set; }
        public string NameColor { get; set; }
        public string RGBA { get; set; }
    }
}
