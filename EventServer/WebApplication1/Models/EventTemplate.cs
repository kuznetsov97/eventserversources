﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventServer.Models
{
    public class EventTemplate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Descripton { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int IdCategory { get; set; }
        public int IdUser { get; set; }
        public int IdTypeRepeat { get; set; }
    }
}
