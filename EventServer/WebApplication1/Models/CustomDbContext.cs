﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventServer.Models
{
    public class CustomDbContext : DbContext
    {
        public CustomDbContext(DbContextOptions<CustomDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<EventTemplate> EventTemplates { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<CategoryEvent> CategoryEvents { get; set; }
        public DbSet<TypeRepeat> TypeRepeats { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
