﻿using EventServer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventServer.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class EventsController : Controller
    {
        private readonly CustomDbContext _context;

        public EventsController(CustomDbContext context)
        {
            _context = context;
        }

        #region Getters

        [HttpPost]
        [Route("geteventsbyinterval")]
        public async Task<IActionResult> GetEventsByInterval([FromBody] Tuple<DateTime, DateTime> dateInterval)
        {
            List<EventTemplate> result = new List<EventTemplate>();
            try
            {
                result = await _context.EventTemplates.Where(x => x.StartDate < dateInterval.Item2 && x.EndDate > dateInterval.Item1).ToListAsync();
                result.AddRange(await _context.Events.Where(x => x.StartDate < dateInterval.Item2 && x.EndDate > dateInterval.Item1).ToListAsync());
            }
            catch
            {
                return BadRequest();
            }

            return Json(result);
        }

        [HttpGet]
        [Route("getoneevent")]
        public async Task<IActionResult> GetOneEvent(int? id)
        {
            if (id == null) return NotFound();

            var @event = await _context.Events.FirstOrDefaultAsync(m => m.Id == id);
            if (@event == null)
                return NotFound();

            return Json(@event);
        }

        [HttpGet]
        [Route("getpastevents")]
        public async Task<IActionResult> GetPastEvents(int count)
        {
            List<Event> result = new List<Event>();
            try
            {
                result.AddRange(await _context.Events.Where(x => x.EndDate < DateTime.Now).ToListAsync());
                result = result.Take(count).ToList();
            }
            catch
            {
                return BadRequest();
            }

            return Json(result);
        }

        [HttpGet]
        [Route("getfutureevents")]
        public async Task<IActionResult> GetFutureEvents(int count)
        {
            List<Event> result = new List<Event>();
            try
            {
                result.AddRange(await _context.Events.Where(x => x.StartDate >= DateTime.Now).ToListAsync());
                result = result.Take(count).ToList();
            }
            catch
            {
                return BadRequest();
            }

            return Json(result);
        }

        /// <summary>
        /// Возвращает базовое событие по его экземпляру
        /// </summary>
        /// <param name="id">Идентификатор экземпляра события</param>
        [HttpGet]
        [Route("getbaseevent")]
        public async Task<IActionResult> BaseEvent(int? id)
        {
            if (id == null)
                return NotFound();

            try
            {
                var baseId = (await _context.Events.FirstOrDefaultAsync(x => x.Id == id))?.IdEventTemplate;

                return Json(await _context.EventTemplates.FirstOrDefaultAsync(x => x.Id == baseId));
            }
            catch
            {
                return BadRequest();
            }
        }

        #endregion

        #region Editors
        /// <summary>
        /// Изменяет только текущий экземпляр события
        /// </summary>
        /// <param name="id">Идентификатор экземпляра события</param>
        [HttpPost]
        [Route("editoneevent")]
        public async Task<IActionResult> Edit([FromBody] Event @event)
        {
            if (@event == null)
            {
                return NotFound();
            }

            var editingevent = _context.Events.FirstOrDefault(x=>x.Id == @event.Id);
            if (editingevent == null)
            {
                return NotFound();
            }
            else
            {
                editingevent.Name = @event.Name;
                editingevent.Descripton = @event.Descripton;
                editingevent.StartDate = @event.StartDate;
                editingevent.EndDate = @event.EndDate;
                editingevent.IdCategory = @event.IdCategory;
            }

            await _context.SaveChangesAsync();

            return Json("success");
        }

        /// <summary>
        /// Изменяет базовое событие и всех его наследников
        /// </summary>
        /// <param name="id">Идентфикатор базового события</param>
        [HttpPost]
        [Route("editgroupevents")]
        public async Task<IActionResult> EditAll([FromBody] EventTemplate @event)
        {
            if (@event == null)
            {
                return NotFound();
            }

            var baseEvent = _context.EventTemplates.FirstOrDefault(x => x.Id == @event.Id);

            if (baseEvent == null)
                return NotFound();

            var startDiff = baseEvent.StartDate - @event.StartDate;
            var endDiff = baseEvent.EndDate - @event.EndDate;

            //Изменение шаблона события
            baseEvent.Name = @event.Name;
            baseEvent.Descripton = @event.Descripton;
            baseEvent.StartDate = @event.StartDate;
            baseEvent.EndDate = @event.EndDate;
            baseEvent.IdCategory = @event.IdCategory;

            await _context.SaveChangesAsync();

            //поиск и изменение экземпляров событий
            var editingevents = _context.Events.Where(x => x.IdEventTemplate == @event.Id);
            if (editingevents == null || editingevents.Count() == 0)
            {
                return NotFound();
            }
            else
            {
                foreach (var itemEvent in editingevents)
                {
                    itemEvent.Name = @event.Name;
                    itemEvent.Descripton = @event.Descripton;
                    itemEvent.StartDate += startDiff;
                    itemEvent.EndDate += endDiff;
                    itemEvent.IdCategory = @event.IdCategory;

                    await _context.SaveChangesAsync();
                }
            }            

            return Json("success");
        }
        #endregion

        /// <summary>
        /// Создает базовое событие и его потомков с заданным интервалом повторений 
        /// </summary>
        /// <param name="event">Базовое событие</param>
        [HttpPost]
        [Route("createevent")]
        public async Task<IActionResult> Create([FromBody] EventTemplate @event)
        {
            try
            {
                _context.EventTemplates.Add(@event);
                await _context.SaveChangesAsync();

                var startDate = @event.StartDate;
                var endDate = @event.EndDate;
                while (endDate < DateTime.MaxValue)
                {
                    _context.Events.Add(new Event()
                    {
                        Descripton = @event.Descripton,
                        Name = @event.Name,
                        StartDate = startDate,
                        EndDate = endDate,
                        IdCategory = @event.IdCategory,
                        IdTypeRepeat = @event.IdTypeRepeat,
                        IdUser = @event.IdUser,
                        IdEventTemplate = @event.Id
                    });

                    await _context.SaveChangesAsync();

                    switch (@event.IdTypeRepeat)
                    {
                        case 2:
                            {
                                startDate = startDate.AddHours(1);
                                endDate = endDate.AddHours(1);
                                break;
                            }
                        case 3:
                            {
                                startDate = startDate.AddDays(1);
                                endDate = endDate.AddDays(1);
                                break;
                            }
                        case 4:
                            {
                                startDate = startDate.AddDays(7);
                                endDate = endDate.AddDays(7);
                                break;
                            }
                        case 5:
                            {
                                startDate = startDate.AddMonths(1);
                                endDate = endDate.AddMonths(1);
                                break;
                            }
                    }                    
                }

                
            }
            catch
            {
                return BadRequest();
            }

            return Json("success");
        }

        /// <summary>
        /// Удаление событий.
        /// Если id.Value == true - удаляются все события одного шаблона
        /// Иначе удаляется только одно выбранное событие (у которого Id == id.Key)
        /// </summary>
        /// <param name="id">идентификатор события</param>
        [HttpPost]
        [Route("delete")]
        public async Task<IActionResult> Delete([FromBody] KeyValuePair<int, bool> id)
        {
            try 
            {
                var baseId = _context.Events.FirstOrDefault(x => x.Id == id.Key)?.IdEventTemplate;
                var baseEvent = _context.EventTemplates.FirstOrDefault(x => x.Id == baseId);

                if (id.Value)
                {
                    var deletings = await _context.Events.Where(x => x.IdEventTemplate == baseId.Value).ToListAsync();
                    _context.EventTemplates.Remove(baseEvent);
                    _context.Events.RemoveRange(deletings);
                }
                else
                {
                    var @event = await _context.Events.FirstOrDefaultAsync(m => m.Id == id.Key);
                    _context.Events.Remove((Event)@event);
                }

                await _context.SaveChangesAsync();
            }
            catch
            {
                return BadRequest();
            }

            return Json("success");
        }
    }
}
